from typing import Any, Iterator, List
from math import log, sqrt
from random import random


class UCBItem:
    def __init__(self, value: Any, exploration_weight: float) -> None:
        self.value = value
        self.ucb1 = 0
        self._reward = 0
        self._attempts = 0
        self._exploration_weight = exploration_weight

    def calculate_ucb(self, total_attempts: int) -> None:
        if self._attempts == 0:
            self.ucb1 = 10
            return
        self.ucb1 = self._reward / self._attempts
        self.ucb1 += self._exploration_weight * sqrt(log(total_attempts) / self._attempts)

    def add_reward(self, reward: float) -> None:
        self._attempts += 1
        self._reward += reward


class UCBList:
    def __init__(self, sort_probability: int = 1, exploration_weight: float = sqrt(2)) -> None:
        # I really don't like that if I want a sorted collection in python,
        # I have to use a list, never update the key, or make it from scratch...
        self.list = []
        self._attempts = 0
        self._sort_probability = sort_probability
        self.exploration_weight = exploration_weight

    def __iter__(self) -> Iterator[List]:
        return self.list.__iter__()

    def add_item(self, value: Any) -> bool:
        for item in self.list:
            if item.value == value:
                return False
        self.list.append(UCBItem(value, self.exploration_weight))
        return True

    def add_reward(self, value: Any, reward: float) -> None:
        if not 0 <= reward <= 1:
            return
        self._attempts += 1
        for item in self.list:
            if item.value == value:
                item.add_reward(reward)
            item.calculate_ucb(self._attempts)
        if random() < self._sort_probability:
            self.list.sort(key=lambda x: -x.ucb1)

    @property
    def best(self) -> Any:
        return self.list[0].value
