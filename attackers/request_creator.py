from typing import List

from environment.node import Node
from environment.message import Request
from environment.action import Action, ActionList, ActionParameter, ActionParameterType
from environment.exploit_store import ExploitStore
from attackers.target_selector import TargetSelector
from attackers.attacker_memory import AttackerMemory


class RequestCreator():

    def __init__(self, me: Node, rit: str):
        self._me = me
        self._rit = rit
        _action_list = ActionList().get_actions("rit")
        self._actions = {}
        for action in _action_list:
            self._actions[action.tags[0].name] = action

    def ready(self) -> bool:
        return True

    def request(self, targeter: TargetSelector = None) -> Request:
        return None


class HostRequester(RequestCreator):

    def request(self, targeter: TargetSelector = None) -> Request:
        action = self._actions[self._rit]
        request = Request(str(targeter.host()), "", self._me.id, action=action, session=targeter.session(), authorization=None)
        return request


class ServiceRequester(RequestCreator):

    def request(self, targeter: TargetSelector = None) -> Request:
        action = self._actions[self._rit]
        request = Request(str(targeter.host()), targeter.service(), self._me.id, action=action, session=targeter.session(), authorization=None)
        return request


class ServiceAuthRequester(RequestCreator):

    def request(self, targeter: TargetSelector = None) -> Request:
        action = self._actions[self._rit]
        request = Request(str(targeter.host()), targeter.service(), self._me.id, action=action, session=targeter.session(), authorization=targeter.auth())
        return request


class ServiceExploitRequester(RequestCreator):

    def request(self, targeter: TargetSelector = None) -> Request:
        action = self._actions[self._rit]
        action.set_exploit(ExploitStore().get_exploit(targeter.exploit())[0])
        request = Request(str(targeter.host()), targeter.service(), self._me.id, action=action, session=targeter.session(), authorization=None)
        return request


class ServiceExploitAuthRequester(RequestCreator):

    def request(self, targeter: TargetSelector = None) -> Request:
        action = self._actions[self._rit]
        action.set_exploit(ExploitStore().get_exploit(targeter.exploit())[0])
        request = Request(str(targeter.host()), targeter.service(), self._me.id, action=action, session=targeter.session(), authorization=targeter.auth())
        return request


class LateralMovementRequester(RequestCreator):

    def request(self, targeter: TargetSelector = None) -> Request:
        action = self._actions[self._rit]
        attacker_class = "ModularAttacker"
        # attacker_class += targeter.class_suffix()
        action.add_parameters(ActionParameter(ActionParameterType.ID, attacker_class))
        request = Request(str(targeter.host()), "", self._me.id, action, session=targeter.session(), authorization=targeter.auth())
        return request
