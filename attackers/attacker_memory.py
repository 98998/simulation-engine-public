from typing import List, Tuple, Union, Optional, Callable, Any
from netaddr import IPAddress  # pylint: disable=unused-wildcard-import
from random import choice

from environment.access import Authorization, AccessLevel
from environment.exploit_store import ExploitStore
from environment.exploit import Exploit, ExploitLocality, ExploitCategory, ExploitParameterType
from environment.node import Data
from environment.views import NodeView
from environment.network_elements import Session
from attackers.ucb import UCBList


class RitAuthCombinations:
    def __init__(self) -> None:
        self._dict = {}  # self._dict[rit] = [Auth]

    def get_missing(self, rit: str, auths: UCBList) -> Optional[Authorization]:
        if rit not in self._dict.keys():
            self._dict[rit] = []
        used = self._dict[rit]

        for auth in auths:
            if auth.value not in used:
                used.append(auth.value)
                return auth.value
        return None


class ExploitInfo:

    def __init__(self, name: str, svc: str) -> None:
        self._name = name
        self._exploit = ExploitStore().get_exploit(id=self._name)[0]
        self._rit = []
        self._tested = RitAuthCombinations()
        self._tested_without_auth = []
        self._add_rits()

    def __str__(self):
        return self._name

    def get_untested_auth_for_rit(self, rit: str, auths: UCBList, no_reuse: List) -> Optional[Authorization]:
        if rit not in self._rit:
            return None
        a2 = UCBList()
        for a in auths:
            if a.value not in no_reuse:
                a2.add_item(a.value)
        return self._tested.get_missing(rit, a2)

    def _add_rits(self) -> None:
        # I think this method should be somewhere else (separate class?)
        # possibly unite some logic with environment/rit.py?

        # no condition needs to hold for these rits:
        self._rit.append("rit:ensure_access:command_and_control")
        self._rit.append("rit:targeted_exploits:exploit_remote_services")

        # information discovery
        if self._exploit.category == ExploitCategory.AUTH_MANIPULATION:
            self._rit.append("rit:active_recon:information_discovery")

        # privilege escalation
        if self._exploit.locality == ExploitLocality.LOCAL and self._exploit.category == ExploitCategory.AUTH_MANIPULATION:

            self._rit.append("rit:privilege_escalation:user_privilege_escalation")
            self._rit.append("rit:privilege_escalation:root_privilege_escalation")

    @property
    def name(self) -> Optional[str]:
        return self._name


class ServiceInfo:
    def __init__(self, name: str, version: str = None) -> None:
        self._name = name
        self.version = version
        self._tested = RitAuthCombinations()
        self._exploits = []
        self.tested_rits = []

    def add_exploit(self, exploit: str) -> bool:
        if any([exploit == e.name for e in self._exploits]):
            return False
        self._exploits.append(ExploitInfo(exploit, self._name))
        return True

    def get_auth(self, rit: str, auths: UCBList) -> Optional[Authorization]:
        return self._tested.get_missing(rit, auths)

    def get_exploit_and_auth(self, rit: str, auths: UCBList, no_reuse: List) -> Tuple[Optional[Exploit], Optional[Authorization]]:
        for ei in self._exploits:
            auth = ei.get_untested_auth_for_rit(rit, auths, no_reuse)
            if auth is not None:
                return (ei.name, auth)
        return None, None


class HostInfo:
    def __init__(self):
        self.services = {}
        self.services_ucb = UCBList()
        self.session = None
        self.auths = UCBList()
        self._rit_tested_auth = {}
        self._elevated = False
        self.infection = AccessLevel.NONE

    def was_auth_tested(self, rit: str, auth: Authorization) -> bool:
        if auth is None:
            return rit in self._rit_tested_auth.keys()
        if rit not in self._rit_tested_auth.keys():
            self._rit_tested_auth[rit] = []
        if auth in self._rit_tested_auth[rit]:
            return True
        else:
            self._rit_tested_auth[rit].append(auth)
            return False

    def add_service(self, service_id: ServiceInfo, version: str = None) -> bool:
        if service_id in self.services.keys():
            return False
        self.services[service_id] = ServiceInfo(service_id, version)
        return True

    def add_auth(self, auth: Authorization) -> None:
        self.auths.add_item(auth)

    def ready_for_rit(self, rit: str) -> bool:
        if rit == "rit:ensure_access:lateral_movement" \
           and (self.infection == AccessLevel.ELEVATED or (self.infection == AccessLevel.LIMITED and not self._elevated)):
            return False
        if rit in ["rit:privilege_escalation:user_privilege_escalation",
                   "rit:privilege_escalation:root_privilege_escalation",
                   "rit:ensure_access:lateral_movement"] \
                and self.session is None:
            return False
        return True


class AttackerMemory:

    def __init__(self) -> None:
        self.hosts = {}
        self.hosts_ucb = UCBList()
        self.data = []
        self.auths = UCBList()
        self.sessions = UCBList()
        self.sessions.add_item(None)
        self._no_reuse = []

    def new_view(self, host: IPAddress, view: NodeView) -> None:
        self.hosts[host].view = view

    def new_session(self, host: Optional[IPAddress], session: Session) -> None:
        if host is not None:
            self.hosts[host].session = session
        for s in self.sessions:
            if s.value == session:
                return
        self.sessions.add_item(session)

    def get_session(self, host: IPAddress) -> Optional[Session]:
        if host not in self.hosts.keys():
            return None
        return self.hosts[host].session

    def service_tested(self, host: IPAddress, service: str, rit: str) -> bool:
        if host not in self.hosts.keys():
            return False
        host = self.hosts[host]
        if service not in host.services.keys():
            return False
        if rit not in host.services[service].tested_rits:
            host.services[service].tested_rits.append(rit)

    def get_data(self, rit: Optional[str],
                 host_condition: Callable,
                 service_condition: Optional[Callable] = None,
                 return_exploit: bool = False,
                 return_auth: bool = False):
        for host_key in self.hosts_ucb:
            host = self.hosts[host_key.value]
            if not host_condition(host):
                continue

            if service_condition is not None:
                for service_key in host.services_ucb:
                    service = host.services[service_key.value]
                    if not service_condition(service):
                        continue
                    if not return_exploit:
                        if not return_auth:
                            return host_key.value, service_key.value
                        else:
                            auth = service.get_auth(rit, host.auths)
                            if auth is not None:
                                return host_key.value, service_key.value, auth
                    else:
                        if not return_auth:
                            for exploit in service._exploits:
                                if rit not in exploit._tested_without_auth:
                                    exploit._tested_without_auth.append(rit)
                                    return host_key.value, service_key.value, exploit._name
                        else:
                            exploit, auth = service.get_exploit_and_auth(rit, host.auths, self._no_reuse)
                            if exploit is not None:
                                return host_key.value, service_key.value, exploit, auth

            elif return_auth:
                for auth in host.auths:
                    if auth.value is not None and not host.was_auth_tested(rit, auth.value):
                        return host_key.value, auth.value
            else:
                if not host.was_auth_tested(rit, None):
                    host._rit_tested_auth[rit] = []
                    return host_key.value

        return_value = [None]
        if service_condition is not None:
            return_value.append(None)
        if return_auth:
            return_value.append(None)
        if return_exploit:
            return_value.append(None)
        return tuple(return_value) if len(return_value) > 1 else None

    def all_exploits(self) -> List[Tuple[IPAddress, str, str]]:
        to_ret = []
        for host_key in self.hosts.keys():
            host = self.hosts[host_key]
            for service_key in host.services.keys():
                service = host.services[service_key]
                for exploit in service._exploits:
                    to_ret.append((host_key, service_key, str(exploit)))
        return to_ret

    def all_auths(self) -> List[Authorization]:
        return list(map(lambda x: x.value, self.auths))

    def all_data(self) -> List[Tuple[IPAddress, Any]]:
        return self.data

    def new_host(self, ip: IPAddress) -> bool:
        if ip in self.hosts.keys():
            return False
        self.hosts_ucb.add_item(ip)
        self.hosts[ip] = HostInfo()
        host = self.hosts[ip]
        for auth in self.auths:
            host.add_auth(auth.value)
        return True

    def new_service(self, ip: IPAddress, service: str) -> bool:
        if ip not in self.hosts.keys():
            return False
        host = self.hosts[ip]
        host.services_ucb.add_item(service)
        return host.add_service(service)

    def new_exploit(self, host_id: IPAddress, service_id: str, exploit: str) -> bool:
        if host_id not in self.hosts.keys():
            return False
        host = self.hosts[host_id]
        if service_id not in host.services.keys():
            return False
        service = host.services[service_id]
        return service.add_exploit(exploit)

    def new_data(self, host_id: IPAddress, data: Union[str, Data, Authorization]) -> bool:
        if host_id not in self.hosts.keys():
            return False

        if (isinstance(data, Authorization)):
            for auth in self.auths:
                if self.auth_compare(data, auth.value):
                    return False
            for host_key in self.hosts.keys():
                host = self.hosts[host_key]
                host.add_auth(data)
            self.auths.add_item(data)
            return True
        data_tuple = (host_id, data)
        if data_tuple not in self.data:
            self.data.append(data_tuple)
        return True

    def add_infected(self, infected_host: IPAddress, access_level: AccessLevel = AccessLevel.NONE) -> None:
        if infected_host in self.hosts.keys():
            self.hosts[infected_host].infection = access_level
    
    def auth_compare(self, a1: Authorization, a2: Authorization) -> bool:
        return (
            a1 is not None and
            a2 is not None and
            a1.identity == a2.identity and
            a1.nodes == a2.nodes and
            a1.services == a2.services and
            a1.access_level == a2.access_level
        )
