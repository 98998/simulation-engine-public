from semver import VersionInfo
from netaddr import IPAddress

from attackers.attacker_memory import AttackerMemory
from environment.access import Authorization
from environment.exploit_store import ExploitStore
from environment.message import MessageType, Response, StatusValue
from environment.node import Data
from attackers.ucb_targeter import UCBSelector


class MessageProcessor:
    def __init__(self, mem: AttackerMemory, rit: str) -> None:
        self._mem = mem
        self._rit = rit

    def process(self, message: Response) -> None:
        if (message.src_ip is not None):
            self._mem.new_host(IPAddress(message.src_ip))
        if (message.session is not None):
            self._mem.new_session(IPAddress(message.src_ip), message.session)
        if (message._src_service is not None):
            self._mem.service_tested(IPAddress(message.src_ip), str(message._src_service), self._rit)

        if (self._rit == "rit:active_recon:service_discovery"):
            if (message.status.value == StatusValue.SUCCESS):
                for svc in message._content:
                    self._mem.new_service(IPAddress(message.src_ip), str(svc))

        if (self._rit == "rit:active_recon:vulnerability_discovery"):
            if (message.status.value == StatusValue.SUCCESS):
                tokens = message._content[0].split('-')
                version = VersionInfo.parse(tokens[-1])
                exploits = ExploitStore().get_exploit(service=message._src_service)
                for exploit in exploits:
                    exploit_service = exploit.services.get(message._src_service, None)
                    if exploit_service.min_version <= version <= exploit_service.max_version:
                        self._mem.new_exploit(IPAddress(message.src_ip), str(message._src_service), exploit.id)

        if self._rit in ["rit:active_recon:information_discovery",
                         "rit:disclosure:data_exfiltration"]:
            if (message.status.value == StatusValue.SUCCESS and message._content is not None):
                for data in message._content:
                    # this also works for authorizations
                    self._mem.new_data(IPAddress(message.src_ip), data)

        if self._rit == "rit:ensure_access:command_and_control":
            if (message.status.value == StatusValue.SUCCESS):
                self._mem.new_view(IPAddress(message.src_ip), message._content)

        if self._rit == "rit:targeted_exploits:exploit_remote_services":
            for session in message.content:
                self._mem.new_session(None, session)

        if self._rit in ["rit:privilege_escalation:user_privilege_escalation",
                         "rit:privilege_escalation:root_privilege_escalation"]:
            if (message.status.value == StatusValue.SUCCESS):
                self._mem.new_data(IPAddress(message.src_ip), message.authorization)

        if (self._rit == "rit:ensure_access:lateral_movement"):
            if (message.status.value == StatusValue.SUCCESS):
                self._mem.add_infected(IPAddress(message.src_ip))

        # adding rewards for actions
        # this is work in progress, works for now but might stop working if the simulator adds more features
        reward = 1 if message.status.value == StatusValue.SUCCESS else 0
        self._mem.hosts_ucb.add_reward(IPAddress(message.src_ip), reward)
        host = self._mem.hosts[IPAddress(message.src_ip)]
        if message.authorization is not None:
            host.auths.add_reward(message.authorization, reward)
        if message.src_service is not None:
            host.services_ucb.add_reward(message.src_service, reward)


class UCBSelectorProcessor(MessageProcessor):
    def __init__(self, selector: UCBSelector, mem: AttackerMemory) -> None:
        self._selector = selector
        self._mem = mem

    def process(self, message: Response) -> None:
        successful = message.status.value == StatusValue.SUCCESS
        target_ip = IPAddress(self._selector._last_scan)
        if successful and target_ip not in self._mem.hosts.keys():
            self._mem.new_host(target_ip)
            self._mem.hosts[target_ip].session = self._selector.session()
        self._selector.scan_result(successful)
