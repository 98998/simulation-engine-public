from typing import Tuple, Optional
from math import sqrt, log
from queue import SimpleQueue

from netaddr import IPNetwork

from environment.action import Action
from environment.access import Authorization
from environment.environment import EnvironmentProxy
from environment.message import Request, Response, MessageType, Status, StatusValue
from environment.network_elements import Session
from environment.node import ActiveService, Node, AccessLevel

from attackers.attacker_memory import AttackerMemory
from attackers.target_selector import *  # pylint: disable=unused-wildcard-import
from attackers.request_creator import *  # pylint: disable=unused-wildcard-import
from attackers.message_processor import *  # pylint: disable=unused-wildcard-import
from attackers.ucb_targeter import *  # pylint: disable=unused-wildcard-import

interesting = []
# interesting = ["10.0.1.2", "10.0.1.3"]


class AttackerSubmodule():
    def __init__(self, targeter: TargetSelector, requester: RequestCreator, processor: MessageProcessor,
                 attacker: Node, priority: float = 0, interface_id: int = 0):
        # attacker's type is Node, because Python has no forward declarations :(
        self._targeter = targeter
        self._requester = requester
        self._reward = 0
        self._attempts = 0
        self._attacker = attacker
        self._processor = processor
        self._priority = priority
        self._moduleId = attacker.add_submodule(self)

    def ucb1(self, n: int) -> float:
        # upper confidence bound stategy for multi-armed bandit problem
        # choosing submodule this way should be asymptotically optimal
        if n == 0:
            return 0
        if self._attempts == 0:
            return 1
        return self._reward / self._attempts + sqrt(2 * log(n) / self._attempts)

    def ready(self) -> bool:
        return self._targeter.ready() and self._requester.ready()

    def act(self) -> None:
        self._attacker.queue_request(self._requester.request(self._targeter), self._moduleId)

    def process(self, message: Response):
        self._processor.process(message)


class ModularAttacker(ActiveService):
    def __init__(self, id: str, init_string: str = "", env: EnvironmentProxy = None) -> None:
        super(ModularAttacker, self).__init__(id, "nobody", env)
        self._env = env
        self._responses = []
        self._submodules = []
        self._memory = AttackerMemory()
        self._queue = SimpleQueue()
        self._request_owner = {}
        self._counter = 0
        self._actions = 0
        self._target_network = IPNetwork("0.0.0.0/0")
        if isinstance(init_string, str):
            self._settings = init_string.strip().split(" ")
        else:
            self._settings = []
        if "elevated" in self._settings:
            self._service_access_level = AccessLevel.ELEVATED
        else:
            self._service_access_level = AccessLevel.LIMITED
        self.add_submodules()

        self._action_mapping = {}
        self._action_attempts = []
        self._action_counter = 0
        self._last_action = None

    def reset(self):
        self._responses = []
        self._submodules = []
        self._memory = AttackerMemory()
        self._queue = SimpleQueue()
        self._request_owner = {}
        self._counter = 0
        self._actions = 0
        self.add_submodules()
        self._target_network = IPNetwork("0.0.0.0/0")

        self._action_mapping = {}
        self._action_attempts = []
        self._action_counter = 0
        self._last_action = None

    @classmethod
    def set_target_network(cls, net: IPNetwork) -> None:
        cls._target_network = net

    def add_submodules(self) -> None:
        ucbs = UCBSelector(self, self._target_network)
        if "no_host_scan" not in self._settings:
            rit = "rit:active_recon:host_discovery"
            AttackerSubmodule(ucbs,
                              HostRequester(self, rit),
                              UCBSelectorProcessor(ucbs, self._memory),
                              self, -3)

        rit = "rit:active_recon:service_discovery"
        AttackerSubmodule(HostSelector(self._memory, rit),
                          HostRequester(self, rit),
                          MessageProcessor(self._memory, rit),
                          self, 0)
        """
        rit = "rit:active_recon:information_discovery"
        AttackerSubmodule(ServiceSelector(self._memory, rit),
                          ServiceRequester(self, rit),
                          MessageProcessor(self._memory, rit),
                          self, 1)
        """
        rit = "rit:active_recon:information_discovery"
        AttackerSubmodule(ServiceExploitSelector(self._memory, rit),
                          ServiceExploitRequester(self, rit),
                          MessageProcessor(self._memory, rit),
                          self, 2)

        rit = "rit:destroy:data_destruction"
        AttackerSubmodule(ServiceAuthSelector(self._memory, rit),
                          ServiceAuthRequester(self, rit),
                          MessageProcessor(self._memory, rit),
                          self, 1)

        rit = "rit:disclosure:data_exfiltration"
        AttackerSubmodule(ServiceAuthSelector(self._memory, rit),
                          ServiceAuthRequester(self, rit),
                          MessageProcessor(self._memory, rit),
                          self, 2)

        rit = "rit:targeted_exploits:exploit_remote_services"
        AttackerSubmodule(ServiceExploitSelector(self._memory, rit),
                          ServiceExploitRequester(self, rit),
                          MessageProcessor(self._memory, rit),
                          self, 2.5)

        rit = "rit:privilege_escalation:root_privilege_escalation"
        AttackerSubmodule(ServiceExploitAuthSelector(self._memory, rit),
                          ServiceExploitAuthRequester(self, rit),
                          MessageProcessor(self._memory, rit),
                          self, 3)

        rit = "rit:ensure_access:command_and_control"
        AttackerSubmodule(ServiceExploitSelector(self._memory, rit),
                          ServiceExploitRequester(self, rit),
                          MessageProcessor(self._memory, rit),
                          self, 4)

        rit = "rit:ensure_access:command_and_control"
        AttackerSubmodule(ServiceAuthSelector(self._memory, rit),
                          ServiceAuthRequester(self, rit),
                          MessageProcessor(self._memory, rit),
                          self, 4)

        rit = "rit:active_recon:vulnerability_discovery"
        AttackerSubmodule(ServiceSelector(self._memory, rit),
                          ServiceRequester(self, rit),
                          MessageProcessor(self._memory, rit),
                          self, 5)

        if "lateral" in self._settings:
            rit = "rit:ensure_access:lateral_movement"
            AttackerSubmodule(LateralMovementSelector(self._memory, rit),
                              LateralMovementRequester(self, rit),
                              MessageProcessor(self._memory, rit),
                              self, 6)

    def add_submodule(self, submodule: AttackerSubmodule) -> int:
        self._submodules.append(submodule)
        return len(self._submodules) - 1

    def queue_request(self, req: Request, owner: int) -> None:
        self._queue.put(req)
        self._request_owner[req.id] = owner

    def process_message(self, message: Response) -> Tuple[bool, int]:
        if message.src_ip in interesting or message.src_ip in list(map(lambda x: IPAddress(x), interesting)):
            print("PROCESSING", message)
        self._responses.append(message)
        if isinstance(message, Request):
            return True, 0

        if message.status.value == StatusValue.SUCCESS or \
           isinstance(self._submodules[self._request_owner[message.id]]._processor, UCBSelectorProcessor):
            self._submodules[self._request_owner[message.id]].process(message)
        return True, 1

    def run(self) -> None:
        if self._queue.empty():
            order = sorted(self._submodules, key=lambda x: 0 - x._priority)
            for submodule in order:
                if not submodule.ready():
                    continue
                submodule.act()
                break
        if not self._queue.empty():
            req = self._queue.get()
            if req.dst_ip in interesting or req.dst_ip in list(map(lambda x: IPAddress(x), interesting)):
                print("SENDING", req)
            self._env.send_request(req)

            # Gather some statistics
            action_index = self._action_mapping.get(req.action.tags[0].name, self._action_counter)
            if action_index == self._action_counter:
                self._action_mapping[req.action.tags[0].name] = action_index
                self._action_counter += 1
                self._action_attempts.append(0)

            self._action_attempts[action_index] += 1

    def action_stats(self):
        return self._action_mapping, self._action_attempts
