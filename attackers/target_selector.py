from netaddr import *  # pylint: disable=unused-wildcard-import
from random import randint, choice
from math import sqrt
from environment.access import Authorization

from environment.node import Node
from attackers.attacker_memory import AttackerMemory


class TargetSelector():

    def __init__(self, mem: AttackerMemory, rit: str):
        self._mem = mem
        self._rit = rit
        self._host = ""
        self._service = ""
        self._auth = None
        self._exploit = None

    def ready(self) -> bool:
        return True

    def host(self) -> IPAddress:
        return self._host

    def service(self) -> str:
        return self._service

    def auth(self) -> Authorization:
        return self._auth

    def exploit(self) -> str:
        return self._exploit

    def session(self) -> str:
        if self._mem is None:
            return None
        return self._mem.get_session(self.host())


""" non-functional class since update; only commented out for now
class RandomSelector(TargetSelector):
    # selecs an ip to check for a possible host
    def __init__(self, attacker: Node, interfaceId: int = 0, multiplesOf: int = 1, mask: int = 0):
        super().__init__(None)
        self._attacker = attacker
        # return only targets whose addresses are multiples of this number:
        self._multiplesOf = multiplesOf

        # find the number of potential targets in the network
        # if mask is not given, assume it's 24
        self._addressCount = attacker.interfaces[0].net.size
        if self._addressCount == 0:
            self._addressCount = 256
        if mask > 0:
            self._addressCount = 2**(32-mask)
        self._addressCount /= self._multiplesOf

        # turn ip adress of the attacker and prefix of network to ints
        # for easier manipulation
        self._netInt = int(attacker.interfaces[interfaceId].net.network)
        self._ipInt = int(attacker.interfaces[interfaceId].ip)

        # memory[integer corresponding to an address] = number of times the address was selected
        # memory[0] = number of times any address was selected + 1
        self.resetMemory()

    def resetMemory(self) -> None:
        self._memory = {}
        self._memory[-1] = 1

    def select(self) -> str:
        target = -1
        # select a target that hasn't been selected too often so far
        # also, make sure the attacker doesn't target itself
        while self._memory[target] * self._addressCount / sqrt(2) >= self._memory[-1] \
            or self._netInt + target*self._multiplesOf == self._ipInt:
            target = randint(0, self._addressCount-1)
            if target not in self._memory:
                self._memory[target] = 0

        self._memory[target] += 1
        self._memory[-1] += 1
        return str(IPAddress(int(self._netInt + target*self._multiplesOf)))
"""


class HostSelector(TargetSelector):
    # selects a host to scan for services
    def ready(self) -> bool:
        self._host = self._mem.get_data(None, lambda host: True)
        return self._host is not None


class ServiceSelector(TargetSelector):
    # selects a service to scan for exploits
    def ready(self) -> bool:
        self._host, self._service = \
            self._mem.get_data(self._rit,
                               lambda host: True,
                               lambda service: self._rit not in service.tested_rits)
        return self._host is not None


class ServiceAuthSelector(TargetSelector):
    # selects a service to scan for exploits

    def ready(self) -> bool:
        self._host, self._service, self._auth = \
            self._mem.get_data(self._rit,
                               lambda host: host.ready_for_rit(self._rit),
                               lambda service: True,
                               return_auth=True)
        return self._host is not None


class ServiceExploitSelector(TargetSelector):
    # selects a service to scan for exploits

    def ready(self) -> bool:
        self._host, self._service, self._exploit = \
            self._mem.get_data(self._rit,
                               lambda host: host.ready_for_rit(self._rit),
                               lambda service: True,
                               return_exploit=True)
        return self._host is not None


class ServiceExploitAuthSelector(TargetSelector):
    # selects a service to scan for exploits

    def ready(self) -> bool:
        self._host, self._service, self._exploit, self._auth = \
            self._mem.get_data(self._rit,
                               lambda host: host.ready_for_rit(self._rit),
                               lambda service: True,
                               return_exploit=True,
                               return_auth=True)
        return self._host is not None


class LateralMovementSelector(TargetSelector):
    def ready(self) -> bool:
        self._host, self._auth = \
            self._mem.get_data(self._rit,
                               lambda host: host.ready_for_rit(self._rit),
                               return_auth=True)
        return self._host is not None
