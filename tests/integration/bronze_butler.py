import unittest
import statistics
from netaddr import *

from attackers.simple import SimpleAttacker
from attackers.random import RandomAttacker, ReductionStrategy
from attackers.modular import ModularAttacker
from environment.access import Authorization, AccessLevel, Policy
from environment.action import ActionList, ActionParameter, ActionParameterType
from environment.environment import Environment, EnvironmentState, EnvironmentProxy, Node
from environment.exploit import Exploit, ExploitLocality, ExploitCategory, VulnerableService, ExploitParameter, ExploitParameterType
from environment.exploit_store import ExploitStore
from environment.message import StatusValue, StatusOrigin, Status
from environment.network import Router, FirewallRule, FirewallPolicy
from environment.network_elements import Endpoint, Route, Session, Hop
from environment.node import PassiveService, NodeView, Data

# The total runtime for evaluation can easily exceed a few hours, so this is the way to selectively turn them on and
# off without the need to scroll all the way down
enabled_tests = {
    "test_0000_cto_scenario_omniscient": True,
    "test_0001_cto_scenario_random": True,
    "test_0002_cto_scenario_modular": True,
    "test_0003_vpn_scenario_omniscient": True,
    "test_0004_vpn_scenario_random": True,
    "test_0005_vpn_scenario_modular": True,
    "test_0006_employee_scenario_omniscient": True,
    "test_0007_employee_scenario_random": True,
    "test_0008_employee_scenario_modular": True,
    "test_0009_simple_scenario_random_strategies": False
}

# Global number of runs for each test
number_of_runs = 1


class BronzeButlerScenarios(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls._env = Environment()

        cls._action_list = ActionList().get_actions("rit")
        cls._actions = {}
        for action in cls._action_list:
            cls._actions[action.tags[0].name] = action

        Policy().reset()

        # List of nodes in the infrastructure:
        # -- DMZ --
        # Email server
        # Web server
        # VPN server
        # -- SRV --
        # Domain controller
        # DB server
        # API server
        # -- PC --
        # Employee
        # CTO
        # -- Routers --
        # Outside network - PerimeterRouter - DMZ - InternalRouter - SRV/PC

        # --- Email server ---------------------------------------------------------------------------------------------
        email_srv = Node("email_srv")

        postfix = PassiveService("postfix", owner="mail", version="3.5.0", local=False)
        bash = PassiveService("bash", owner="bash", version="5.0.0", local=True, service_access_level=AccessLevel.LIMITED)
        email_srv.add_service(postfix, bash)

        cls._env.add_node(email_srv)

        # --- Web server -----------------------------------------------------------------------------------------------
        web_srv = Node("web_srv")

        iis = PassiveService("iis", owner="Administrator", version="8.5.0")
        remote_desktop = PassiveService("rdp", owner="Administrator", version="10.0.17666", local=False)
        remote_desktop.set_enable_session(True)
        powershell = PassiveService("powershell", owner="iis", version="6.2.2", local=True)
        emp_dc_auth = Authorization("employee", ["dc_srv"], ["powershell", "rdp"], access_level=AccessLevel.LIMITED)
        Policy().add_authorization(emp_dc_auth)
        powershell.add_private_authorization(emp_dc_auth)
        web_srv.add_service(iis, remote_desktop, powershell)
        web_srv.set_shell(powershell)

        cls._env.add_node(web_srv)

        # --- VPN server -----------------------------------------------------------------------------------------------
        vpn_srv = Node("vpn_srv")

        skysea = PassiveService("skysea_client_view", owner="skysea", version="11.221.3", local=False)
        skysea.set_enable_session(True)
        vpn_srv.add_service(skysea, bash)

        cls._env.add_node((vpn_srv))

        # --- Domain controller ----------------------------------------------------------------------------------------
        dc_srv = Node("dc_srv")

        # Technically this is a number of services, but we are only going to allow remote connection and acquisition of
        # golden tickets. So we may as well keep it at two services
        windows_server = PassiveService("windows_server_2019", owner="Administrator", version="10.0.17666", local=True)

        powershell_dc = PassiveService("powershell", owner="web_srv", version="6.2.2", local=True)
        dc_srv.add_service(windows_server, remote_desktop, powershell_dc)
        dc_srv.set_shell(powershell_dc)

        cls._env.add_node(dc_srv)

        # --- DB server ------------------------------------------------------------------------------------------------
        db_srv = Node("db_srv")

        mssql = PassiveService("mssql", owner="mssql", version="2019.0.0", local=False)
        db_srv.add_service(mssql)

        # Create secret data
        secret = Data(None, owner="cto", description="Secret data")
        mssql.add_private_data(secret)

        cls._env.add_node(db_srv)

        # --- API server -----------------------------------------------------------------------------------------------
        api_srv = Node("api_srv")

        # adding a powershell. This is the same as the one on the web srv, because employee credentials can be extracted
        # from the domain
        api_srv.add_service(skysea, powershell)
        cls._env.add_node(api_srv)

        # --- Employee PC ----------------------------------------------------------------------------------------------
        emp_pc = Node("emp_pc")

        # Probably... skysea is only described in japan, so it is hard to guess how the infrastructure is organized
        powershell = PassiveService("powershell", owner="employee", version="6.2.2", local=True)
        emp_pc.add_service(skysea, powershell)
        emp_pc.set_shell(powershell)

        emp_web_auth = Authorization("employee", ["web_srv"], ["rdp"], access_level=AccessLevel.LIMITED)
        powershell.add_private_authorization(emp_web_auth)
        Policy().add_authorization(emp_web_auth)

        cls._env.add_node(emp_pc)

        # --- CTO PC ---------------------------------------------------------------------------------------------------
        cto_pc = Node("cto_pc")

        # Create a far reaching authorization for the CTO (for every service in the infrastructure), which can be
        # extracted from the shell using exploit
        cto_auth = Authorization("cto", ["*"], ["*"], access_level=AccessLevel.LIMITED)
        Policy().add_authorization(cto_auth)

        # Different instance of powershell needed, because this one contains authorizations
        powershell2 = PassiveService("powershell", owner="cto", version="6.2.2", local=True)
        powershell2.add_private_authorization(cto_auth)
        cto_pc.add_service(powershell2)

        cls._env.add_node(cto_pc)

        # --- Infrastructure router ------------------------------------------------------------------------------------
        # Infrastructure router connects three networks: DMZ, SRV and PC, each a separate 10.0.0.0/24 network
        # Routing within networks is unrestricted, cross-network routing is explicit
        infrastructure_router = Router("infrastructure_router", cls._env)

        # DMZ
        web_srv_port = infrastructure_router.add_port("10.0.0.1", "255.255.255.0")
        email_srv_port = infrastructure_router.add_port("10.0.0.1", "255.255.255.0")
        vpn_srv_port = infrastructure_router.add_port("10.0.0.1", "255.255.255.0")

        cls._env.add_connection(web_srv, infrastructure_router, target_port_index=web_srv_port)
        cls._env.add_connection(email_srv, infrastructure_router, target_port_index=email_srv_port)
        cls._env.add_connection(vpn_srv, infrastructure_router, target_port_index=vpn_srv_port)

        # SRV
        dc_srv_port = infrastructure_router.add_port("10.0.1.1", "255.255.255.0")
        db_srv_port = infrastructure_router.add_port("10.0.1.1", "255.255.255.0")
        api_srv_port = infrastructure_router.add_port("10.0.1.1", "255.255.255.0")

        cls._env.add_connection(dc_srv, infrastructure_router, target_port_index=dc_srv_port)
        cls._env.add_connection(db_srv, infrastructure_router, target_port_index=db_srv_port)
        cls._env.add_connection(api_srv, infrastructure_router, target_port_index=api_srv_port)

        # PC
        cto_pc_port = infrastructure_router.add_port("10.0.2.1", "255.255.255.0")
        emp_pc_port = infrastructure_router.add_port("10.0.2.1", "255.255.255.0")

        cls._env.add_connection(cto_pc, infrastructure_router, target_port_index=cto_pc_port)
        cls._env.add_connection(emp_pc, infrastructure_router, target_port_index=emp_pc_port)

        # Explicit routes:
        # SRV -> DMZ
        infrastructure_router.add_routing_rule(FirewallRule(IPNetwork("10.0.1.1/24"), IPNetwork("10.0.0.1/24"), "*", FirewallPolicy.ALLOW))
        # DMZ -> SRV (not the wisest thing to do, but we don't want to make the scenario too complicated)
        infrastructure_router.add_routing_rule(FirewallRule(IPNetwork("10.0.0.1/24"), IPNetwork("10.0.1.1/24"), "*", FirewallPolicy.ALLOW))
        # PC -> DMZ
        infrastructure_router.add_routing_rule(FirewallRule(IPNetwork("10.0.2.1/24"), IPNetwork("10.0.0.1/24"), "*", FirewallPolicy.ALLOW))
        # cto_pc -> anywhere
        infrastructure_router.add_routing_rule(FirewallRule(IPNetwork("10.0.2.2/32"), IPNetwork("10.0.0.1/16"), "*", FirewallPolicy.ALLOW))

        cls._env.add_node(infrastructure_router)

        # --------------------------------------------------------------------------------------------------------------
        # List of nodes on the outside (represented as a 10.9.0.0/16 network):
        # Attacker
        # Partner

        # --- Attacker -------------------------------------------------------------------------------------------------
        attacker_node = Node("attacker_node")

        env_proxy = EnvironmentProxy(cls._env, "attacker_node")

        cls._attacker = SimpleAttacker("attacker_omniscient", env=env_proxy)
        attacker_node.add_service(cls._attacker)

        cls._random_attacker = RandomAttacker("attacker_random", env=env_proxy)
        attacker_node.add_service(cls._random_attacker)

        cls._modular_attacker = ModularAttacker("attacker_modular", env=env_proxy)
        attacker_node.add_service(cls._modular_attacker)

        cls._env.add_node(attacker_node)

        cls._env.add_pause_on_response("attacker_node.attacker_omniscient")

        # --- Partner PC ----------------------------------------------------------------------------------------------
        partner_pc = Node("partner_pc")

        partner_pc.add_service(skysea, powershell)
        cls._env.add_node(partner_pc)

        # Add an existing session from partner_pc to the api_srv
        cls._env.add_session("partner", ["partner_pc", "perimeter_router", "vpn_srv", "infrastructure_router", "api_srv"], service="skysea_client_view")

        # --- Perimeter router -----------------------------------------------------------------------------------------
        # Router connects DMZ with the outside world. For easier simulation, the outside is modelled as local to
        # the perimeter router
        perimeter_router = Router("perimeter_router", cls._env)

        attacker_port = perimeter_router.add_port("10.9.0.1", "255.255.255.0")
        partner_port = perimeter_router.add_port("10.9.0.1", "255.255.255.0")
        outside_web_srv_port = perimeter_router.add_port("10.9.0.1", "255.255.255.0")
        outside_email_srv_port = perimeter_router.add_port("10.9.0.1", "255.255.255.0")
        outside_vpn_srv_port = perimeter_router.add_port("10.9.0.1", "255.255.255.0")

        cls._env.add_connection(attacker_node, perimeter_router, target_port_index=attacker_port)
        cls._env.add_connection(partner_pc, perimeter_router, target_port_index=partner_port)
        cls._env.add_connection(web_srv, perimeter_router, target_port_index=outside_web_srv_port)
        cls._env.add_connection(email_srv, perimeter_router, target_port_index=outside_email_srv_port)
        cls._env.add_connection(vpn_srv, perimeter_router, target_port_index=outside_vpn_srv_port)

        cls._env.add_node(perimeter_router)

        # Connect the routers together and enable one-way communication between infrastructure router and the DMZ router
        ir_port = infrastructure_router.add_port()
        pr_port = perimeter_router.add_port()

        cls._env.add_connection(infrastructure_router, perimeter_router, source_port_index=ir_port, target_port_index=pr_port)
        infrastructure_router.add_route(Route(IPNetwork("10.9.0.1/24"), ir_port))

        # Uncomment to display the resulting network
        # cls._env.network.render()

        # --------------------------------------------------------------------------------------------------------------
        # Prepare available exploits, which the attacker can use
        # This is an exploit for credentials dumping
        e1 = Exploit("e1", [VulnerableService("powershell", "0.0.0", "6.2.2")], ExploitLocality.LOCAL, ExploitCategory.AUTH_MANIPULATION)

        # This is an exploit to get root access on any powershell-powered machine
        p2_1 = ExploitParameter(ExploitParameterType.ENABLE_ELEVATED_ACCESS, value="TRUE", immutable=True)
        p2_2 = ExploitParameter(ExploitParameterType.IDENTITY, value="Administrator", immutable=True)
        p2_3 = ExploitParameter(ExploitParameterType.IMPACT_SERVICE, value="ALL", immutable=True)
        e2 = Exploit("e2", [VulnerableService("powershell", "0.0.0", "6.2.2")], ExploitLocality.LOCAL,
                     ExploitCategory.AUTH_MANIPULATION, p2_1, p2_2, p2_3)

        # This is an exploit to get golden kerberos ticket at the DC controller
        p3_1 = ExploitParameter(ExploitParameterType.ENABLE_ELEVATED_ACCESS, value="TRUE", immutable=True)
        p3_2 = ExploitParameter(ExploitParameterType.IMPACT_IDENTITY, value="ALL", immutable=True)
        p3_3 = ExploitParameter(ExploitParameterType.IMPACT_NODE, value="ALL", immutable=True)
        p3_4 = ExploitParameter(ExploitParameterType.IMPACT_SERVICE, value="ALL", immutable=True)
        e3 = Exploit("e3", [VulnerableService("windows_server_2019", "0.0.0", "10.0.17666")], ExploitLocality.LOCAL,
                     ExploitCategory.AUTH_MANIPULATION, p3_1, p3_2, p3_3, p3_4)

        # This is an exploit of the skysea client view
        e4 = Exploit("e4", [VulnerableService("skysea_client_view", "0.0.0", "11.221.3")], ExploitLocality.LOCAL, ExploitCategory.CODE_EXECUTION)

        ExploitStore().clear()
        ExploitStore().add_exploit(e1, e2, e3, e4)

        cls._env.init()

        # This is the minimum number of actions, which is needed to successfully finish each of those three variants
        # of the Bronze butler scenario. They are used to compute the normalized action requirements
        cls._min_actions_cto = 2
        cls._min_actions_vpn = 6
        cls._min_actions_employee = 7

        # These are the values of particular runs for each scenario used to count the median
        cls._totals_random_cto = []
        cls._totals_random_vpn = []
        cls._totals_random_employee = []

        cls._totals_modular_cto = []
        cls._totals_modular_vpn = []
        cls._totals_modular_employee = []

    @staticmethod
    def rounded_div(dividend: int, divisor: int):
        return (dividend + divisor // 2) // divisor

    def test_0000_cto_scenario_omniscient(self) -> None:

        # short-circuiting the test
        if not enabled_tests["test_0000_cto_scenario_omniscient"]:
            return

        # Scenario prerequisite - the attacker successfully spearfished the CTO and got a session onto their PC
        #                       - the session is manually generated because spearphishng is not implemented as is
        #                         requires user modelling
        #                       - routing issues (i.e. not being able to pass from perimeter router to infrastructure
        #                         router) are resolved by the session, which models reverse shell following the
        #                         spearphishing exploit
        s = self._env.add_session("attacker", ["attacker_node", "perimeter_router", "infrastructure_router", "cto_pc"])

        # Scenario unfolding:
        # - T1003 Credentials Dumping (extracting authorization of CTO from the local system)
        # - T1018 Remote System Discovery (scanning of other infrastructure networks, identifying services)
        # -       Connection to the DB server
        # - T1005 Data from local system (access to private data using CTO's credentials

        # --------------------------------------------------------------------------------------------------------------
        # Test to show that without session an internal IP is not accessible
        action = self._actions["rit:active_recon:host_discovery"]
        self._attacker.execute_action("10.0.3.2", "", action)

        self._env.run()

        message = self._attacker.get_last_response()
        self.assertEqual(message.status, Status(StatusOrigin.NETWORK, StatusValue.FAILURE))

        # TODO: Missing meta action for getting information about the current node, namely the IP address and the shell
        # Right now I pretend I know it

        # --------------------------------------------------------------------------------------------------------------
        # Credentials Dumping
        action = self._actions["rit:active_recon:information_discovery"]

        e = ExploitStore().get_exploit(service="powershell", category=ExploitCategory.AUTH_MANIPULATION)[0]
        action.set_exploit(e)

        self._attacker.execute_action("10.0.2.2", "powershell", action, session=s)

        self._env.run()

        message = self._attacker.get_last_response()

        self.assertEqual(message.status, Status(StatusOrigin.SERVICE, StatusValue.SUCCESS))

        cto_auth = None
        for a in message.content:
            if a.identity == "cto":
                cto_auth = a
                break

        self.assertIsNotNone(cto_auth, "Got authorization for CTO")
        # --------------------------------------------------------------------------------------------------------------
        # Remote System Discovery
        #
        # Get a list of hosts reachable from the CTO's computer
        action = self._actions["rit:active_recon:host_discovery"]

        nets_to_scan = [IPNetwork("10.0.0.0/24"), IPNetwork("10.0.1.0/24"), IPNetwork("10.0.2.0/24")]

        live_machines = []
        for net in nets_to_scan:
            for host in net.iter_hosts():
                self._attacker.execute_action(str(host), "", action, session=s)
                result, _ = self._env.run()

                self.assertTrue(result, "Environment correctly processed the request")

                message = self._attacker.get_last_response()

                if message.status == Status(StatusOrigin.NODE, StatusValue.SUCCESS):
                    live_machines.append(message.src_ip)

        # Identify services on accessible machines and look for MSSQL server
        action = self._actions["rit:active_recon:service_discovery"]
        mssql_ip = None

        for machine in live_machines:
            self._attacker.execute_action(str(machine), "", action, session=s)
            result, _ = self._env.run()

            self.assertTrue(result, "Environment correctly processed the request")

            message = self._attacker.get_last_response()

            if "mssql" in message.content:
                mssql_ip = machine
                break

        self.assertIsNotNone(mssql_ip, "Got the IP of the DB server")

        # --------------------------------------------------------------------------------------------------------------
        # Data exfiltration
        action = self._actions["rit:disclosure:data_exfiltration"]

        self._attacker.execute_action(str(mssql_ip), "mssql", action, session=s, authorization=cto_auth)

        result, _ = self._env.run()

        self.assertTrue(result, "Environment correctly processed the request")

        message = self._attacker.get_last_response()

        data_found = False
        for datum in message.content:
            if datum.description == "Secret data":
                data_found = True
            break

        self.assertTrue(data_found, "Successfully extracted data from the DB")

    def test_0001_cto_scenario_random(self):

        # Configuration for evaluation
        filename = "cto_scenario_random.data"

        # Short-circuit scenario
        if not enabled_tests["test_0001_cto_scenario_random"]:
            return

        output = open(filename, "w")

        s = self._env.add_session("attacker", ["attacker_node", "perimeter_router", "infrastructure_router", "cto_pc"])

        # --------------------------------------------------------------------------------------------------------------
        # Let random attacker attempt the complex scenario
        for i in range(0, number_of_runs):
            self._random_attacker.reset()
            # --------------------------------------------------------------------------------------------------------------
            # Let random attacker attempt the simple scenario
            self._random_attacker.set_action_limit(150000)
            self._random_attacker.set_action_namespace("rit")
            self._random_attacker.add_targets(IPNetwork("10.0.0.0/24"))
            self._random_attacker.add_targets(IPNetwork("10.0.1.0/24"))
            self._random_attacker.add_targets(IPNetwork("10.0.2.0/24"))
            self._random_attacker.add_sessions(s)
            self._random_attacker.set_services("postfix", "bash", "apache", "skysea_client_view",
                                               "windows_server_2019",
                                               "rdp", "mssql", "powershell")

            self._random_attacker.set_reduction_strategy(ReductionStrategy.LIVE_TARGETS_ONLY)
            self._random_attacker.set_goal("Secret data")

            self._random_attacker.run()
            self._env.run()

            total = self._random_attacker.attack_stats()[0].total
            print("1 {} {}".format(str(total), str(self.rounded_div(total, self._min_actions_cto))), file=output)
            print("CTO scenario [random attacker]: run no. {}/{}, total actions: {}".format(str(i+1), str(number_of_runs), str(total)))
            self._totals_random_cto.append(total)

        output.close()

        # Store median in temporary file
        median_output = open("median_random_1.data", "w")
        median_random_cto = statistics.median_low(self._totals_random_cto)
        print("1 {} {}".format(str(median_random_cto), str(self.rounded_div(median_random_cto, self._min_actions_cto))), file=median_output)
        median_output.close()

    def test_0002_cto_scenario_modular(self):

        # Configuration for evaluation
        filename = "cto_scenario_modular.data"

        # Short-circuit scenario
        if not enabled_tests["test_0002_cto_scenario_modular"]:
            return

        # give the attacker the session and IP of a target
        attacked_ips = ["10.0.2.2"]
        s = self._env.add_session("attacker", ["attacker_node", "perimeter_router", "infrastructure_router", "cto_pc"])

        output = open(filename, "w")

        test_mapping = None
        test_attempts = {}
        test_totals = []

        for i in range(0, number_of_runs):
            self._modular_attacker.reset()

            for ip in attacked_ips:
                self._modular_attacker._memory.new_host(IPAddress(ip))
                self._modular_attacker._memory.new_session(IPAddress(ip), s)

            # tell the attacker IPs of the routers - sadly currently unobtainable via simulator
            self._modular_attacker._submodules[0]._targeter.add_router_manually(IPNetwork("10.0.0.1/24"))
            self._modular_attacker._submodules[0]._targeter.add_router_manually(IPNetwork("10.0.1.1/24"))
            self._modular_attacker._submodules[0]._targeter.add_router_manually(IPNetwork("10.0.2.1/24"))

            # let the attacker do its thing until it finds the secret data
            actions = 0
            while (True):
                actions += 1
                self._modular_attacker.run()
                _, _ = self._env.run() if self._env._state == EnvironmentState.PAUSED else self._env.run()
                if len(self._modular_attacker._memory.all_data()) > 0:
                    break

            print("2 {} {}".format(str(actions), str(self.rounded_div(actions, self._min_actions_cto))), file=output)
            print("CTO scenario [modular attacker]: run no. {}/{}, total actions: {}".format(str(i+1), str(number_of_runs), str(actions)))
            self._totals_modular_cto.append(actions)

            # Get some statistics on action ratios
            mapping, attempts = self._modular_attacker.action_stats()

            if test_mapping is None:
                test_mapping = mapping

            if len(mapping) > len(test_mapping):
                raise Exception("Got longer mapping than before")

            for j in range(0, len(test_mapping)):
                if j not in test_attempts:
                    test_attempts[j] = []
                test_attempts[j].append(0)

            for item, index in mapping.items():
                test_attempts[test_mapping[item]][-1] = attempts[index]

            test_totals.append(actions)

        output.close()

        # Store median in temporary file
        median_output = open("median_modular_1.data", "w")
        median_modular_cto = statistics.median_low(self._totals_modular_cto)
        print("2 {} {}".format(str(median_modular_cto), str(self.rounded_div(median_modular_cto, self._min_actions_cto))), file=median_output)
        median_output.close()

        # Store action distribution in a temporary file
        action_distribution_cto = open("action_distribution_cto.data", "w")

        reverse_mapping = [None] * len(test_mapping)
        for k, v in test_mapping.items():
            reverse_mapping[v] = k

        for i in range(0, len(reverse_mapping)):
            action_distribution_cto.write(reverse_mapping[i] + " ")
        action_distribution_cto.write("\n")

        for j in range(0, number_of_runs):
            for i in range(0, len(test_mapping)):
                action_distribution_cto.write(str(test_attempts[i][j] / test_totals[j]) + " ")
            action_distribution_cto.write("\n")

        action_distribution_cto.close()

    def test_0003_vpn_scenario_omniscient(self) -> None:
        # short-circuiting the test
        if not enabled_tests["test_0003_vpn_scenario_omniscient"]:
            return

        # Scenario prerequisite - the attacker successfully spearfished the partner and got a session onto their PC
        #                       - the session is manually generated because spearphishng is not implemented as is
        #                         requires user modelling
        #                       - routing issues (i.e. not being able to pass from perimeter router to infrastructure
        #                         router) are resolved by the session, which models reverse shell following the
        #                         spearphishing exploit
        s = self._env.add_session("attacker", ["attacker_node", "perimeter_router", "partner_pc"])

        # Scenario unfolding:
        # - xxx   Exploiting skysea client to get access to API server
        # - T1003 Credentials Dumping (extracting authorization of employee to the dc from the local system)
        # - T1018 Remote System Discovery (scanning of other infrastructure networks, identifying services, discovering
        #         path to SRV network segment)
        # -       Connection to the dc
        # - T1088 User privilege escalation to Administrator account
        # - T1097 Creating the golden ticket
        # -       Connection to the db server using the golden ticket
        # - T1005 Data from local system (access to private data using CTO's credentials

        # --------------------------------------------------------------------------------------------------------------
        # Exploit of the skysea client and get the session to the API server
        action = self._actions["rit:targeted_exploits:exploit_remote_services"]

        e = ExploitStore().get_exploit(service="skysea_client_view", category=ExploitCategory.CODE_EXECUTION)[0]
        action.set_exploit(e)

        self._attacker.execute_action("10.9.0.3", "skysea_client_view", action, session=s)

        self._env.run()

        message = self._attacker.get_last_response()
        api_session = message.content[0]

        self.assertEqual(message.status, Status(StatusOrigin.SERVICE, StatusValue.SUCCESS))
        self.assertEqual(api_session.endpoint.id, "api_srv", "Got a session to the API server")

        # --------------------------------------------------------------------------------------------------------------
        # Credentials Dumping
        action = self._actions["rit:active_recon:information_discovery"]

        e = ExploitStore().get_exploit(service="powershell", category=ExploitCategory.AUTH_MANIPULATION)[0]
        action.set_exploit(e)

        # TODO: Sessions either need to carry information about IP addresses they are traversing, or that meta action
        #       for listing network information must be added. Right now, this is a bit of a cheating, but the
        #       attacker is omniscient, so...
        self._attacker.execute_action("10.0.1.4", "powershell", action, session=api_session)

        self._env.run()

        message = self._attacker.get_last_response()

        self.assertEqual(message.status, Status(StatusOrigin.SERVICE, StatusValue.SUCCESS))

        dc_auth = None
        for a in message.content:
            if a.identity == "employee":
                dc_auth = a
                break

        self.assertIsNotNone(dc_auth, "Got DC authorization for an employee")

        # --------------------------------------------------------------------------------------------------------------
        # Remote System Discovery
        #
        # Get a list of hosts reachable from the web server (it should be different than from an employee computer)
        action = self._actions["rit:active_recon:host_discovery"]

        nets_to_scan = [IPNetwork("10.0.0.0/24"), IPNetwork("10.0.1.0/24"), IPNetwork("10.0.2.0/24")]

        live_machines = []
        for net in nets_to_scan:
            for host in net.iter_hosts():
                self._attacker.execute_action(str(host), "", action, session=api_session)
                result, _ = self._env.run()

                self.assertTrue(result, "Environment correctly processed the request")

                message = self._attacker.get_last_response()

                if message.status == Status(StatusOrigin.NODE, StatusValue.SUCCESS):
                    live_machines.append(message.src_ip)

        # Identify services on accessible machines
        action = self._actions["rit:active_recon:service_discovery"]
        mssql_ip = None
        dc_ip = None

        for machine in live_machines:
            self._attacker.execute_action(str(machine), "", action, session=api_session)
            result, _ = self._env.run()

            self.assertTrue(result, "Environment correctly processed the request")

            message = self._attacker.get_last_response()

            if "mssql" in message.content:
                mssql_ip = machine

            elif "windows_server_2019" in message.content:
                dc_ip = machine

        self.assertIsNotNone(mssql_ip, "The DB server is now accessible")
        self.assertIsNotNone(dc_ip, "The DC server is now accessible")

        # --------------------------------------------------------------------------------------------------------------
        # Connection to the DC using employee authentication
        action = self._actions["rit:ensure_access:command_and_control"]

        self._attacker.execute_action(str(dc_ip), "rdp", action, authorization=dc_auth, session=api_session)

        result, _ = self._env.run()
        self.assertTrue(result, "Environment correctly processed the request")

        message = self._attacker.get_last_response()
        # New session established to the DC server
        dc_session = message.session

        self.assertEqual(dc_session.endpoint.id, "dc_srv", "Successfully connected to the web server")

        # --------------------------------------------------------------------------------------------------------------
        # Root privilege escalation to get full access to the DC
        action = self._actions["rit:privilege_escalation:root_privilege_escalation"]

        exploits = ExploitStore().get_exploit(service="powershell", category=ExploitCategory.AUTH_MANIPULATION)
        exploit = None

        # Get the exploit which enables elevated access
        for e in exploits:
            for p in e.parameters.values():
                if p.exploit_type == ExploitParameterType.ENABLE_ELEVATED_ACCESS and p.value == "TRUE":
                    exploit = e
                    break

        self.assertIsNotNone(exploit, "Found an exploit which enables elevation to root privileges")

        action.set_exploit(exploit)
        self._attacker.execute_action(str(dc_ip), "powershell", action, authorization=dc_auth, session=dc_session)

        result, _ = self._env.run()
        self.assertTrue(result, "Environment correctly processed the request")

        message = self._attacker.get_last_response()

        dc_root_auth = message.authorization

        self.assertEqual(dc_root_auth.identity, "Administrator", "Got administrative access to the dc server")

        # --------------------------------------------------------------------------------------------------------------
        # Get root access to the entire infrastructure
        action = self._actions["rit:privilege_escalation:root_privilege_escalation"]

        e = ExploitStore().get_exploit(service="windows_server_2019", category=ExploitCategory.AUTH_MANIPULATION)[0]
        action.set_exploit(e)

        self._attacker.execute_action(str(dc_ip), "windows_server_2019", action, authorization=dc_root_auth,
                                      session=dc_session)

        result, _ = self._env.run()
        self.assertTrue(result, "Environment correctly processed the request")

        message = self._attacker.get_last_response()

        infrastructure_root_auth = message.authorization

        self.assertEqual(infrastructure_root_auth.nodes, ["*"], "Got administrative access to the infrastructure")

        # --------------------------------------------------------------------------------------------------------------
        # Data exfiltration
        action = self._actions["rit:disclosure:data_exfiltration"]

        self._attacker.execute_action(str(mssql_ip), "mssql", action, session=api_session,
                                      authorization=infrastructure_root_auth)

        result, _ = self._env.run()

        self.assertTrue(result, "Environment correctly processed the request")

        message = self._attacker.get_last_response()

        data_found = False
        for datum in message.content:
            if datum.description == "Secret data":
                data_found = True
            break

        self.assertTrue(data_found, "Successfully extracted data from the DB")

    def test_0004_vpn_scenario_random(self) -> None:
        # Configuration for evaluation
        filename = "vpn_scenario_random.data"

        # Short-circuit scenario
        if not enabled_tests["test_0004_vpn_scenario_random"]:
            return

        output = open(filename, "w")

        s = self._env.add_session("attacker", ["attacker_node", "perimeter_router", "partner_pc"])

        # --------------------------------------------------------------------------------------------------------------
        # Let random attacker attempt the complex scenario
        for i in range(0, number_of_runs):
            self._random_attacker.reset()
            # --------------------------------------------------------------------------------------------------------------
            # Let random attacker attempt the simple scenario
            self._random_attacker.set_action_limit(150000)
            self._random_attacker.set_action_namespace("rit")
            self._random_attacker.add_targets(IPNetwork("10.0.0.0/24"))
            self._random_attacker.add_targets(IPNetwork("10.0.1.0/24"))
            self._random_attacker.add_targets(IPNetwork("10.0.2.0/24"))
            self._random_attacker.add_targets(IPNetwork("10.9.0.3/32"))
            self._random_attacker.add_sessions(s)
            self._random_attacker.set_services("postfix", "bash", "apache", "skysea_client_view",
                                               "windows_server_2019",
                                               "rdp", "mssql", "powershell")

            self._random_attacker.set_reduction_strategy(ReductionStrategy.LIVE_TARGETS_ONLY)
            self._random_attacker.set_goal("Secret data")

            self._random_attacker.run()
            self._env.run()

            total = self._random_attacker.attack_stats()[0].total
            print("3 {} {}".format(str(total), str(self.rounded_div(total, self._min_actions_vpn))), file=output)
            print("VPN scenario [random attacker]: run no. {}/{}, total actions: {}".format(str(i+1), str(number_of_runs), str(total)))
            self._totals_random_vpn.append(total)

        output.close()

        # Store median in temporary file
        median_output = open("median_random_2.data", "w")
        median_random_vpn = statistics.median_low(self._totals_random_vpn)
        print("3 {} {}".format(str(median_random_vpn), str(self.rounded_div(median_random_vpn, self._min_actions_vpn))), file=median_output)
        median_output.close()

    def test_0005_vpn_scenario_modular(self) -> None:
        # Configuration for evaluation
        filename = "vpn_scenario_modular.data"

        # Short-circuit scenario
        if not enabled_tests["test_0005_vpn_scenario_modular"]:
            return

        output = open(filename, "w")

        # give the attacker the session and IP of a target
        attacked_ips = ["10.9.0.3"]
        s = self._env.add_session("attacker", ["attacker_node", "perimeter_router", "partner_pc"])

        test_mapping = None
        test_attempts = {}
        test_totals = []

        # --------------------------------------------------------------------------------------------------------------
        # Let random attacker attempt the complex scenario
        for i in range(0, number_of_runs):
            self._modular_attacker.reset()

            for ip in attacked_ips:
                self._modular_attacker._memory.new_host(IPAddress(ip))
                self._modular_attacker._memory.new_session(IPAddress(ip), s)

            # tell the attacker IPs of the routers - sadly currently unobtainable via simulator
            self._modular_attacker._submodules[0]._targeter.add_router_manually(IPNetwork("10.0.0.1/24"))
            self._modular_attacker._submodules[0]._targeter.add_router_manually(IPNetwork("10.0.1.1/24"))
            self._modular_attacker._submodules[0]._targeter.add_router_manually(IPNetwork("10.0.2.1/24"))

            # let the attacker do its thing until it finds the secret data
            actions = 0
            while actions < 150000:
                actions += 1
                self._modular_attacker.run()
                _, _ = self._env.run() if self._env._state == EnvironmentState.PAUSED else self._env.run()
                if len(self._modular_attacker._memory.all_data()) > 0:
                    break

            print("4 {} {}".format(str(actions), str(self.rounded_div(actions, self._min_actions_vpn))), file=output)
            print("VPN scenario [modular attacker]: run no. {}/{}, total actions: {}".format(str(i+1), str(number_of_runs), str(actions)))
            self._totals_modular_vpn.append(actions)
            # Get some statistics on action ratios
            mapping, attempts = self._modular_attacker.action_stats()

            if test_mapping is None:
                test_mapping = mapping

            if len(mapping) > len(test_mapping):
                raise Exception("Got longer mapping than before")

            for j in range(0, len(test_mapping)):
                if j not in test_attempts:
                    test_attempts[j] = []
                test_attempts[j].append(0)

            for item, index in mapping.items():
                test_attempts[test_mapping[item]][-1] = attempts[index]

            test_totals.append(actions)

        output.close()

        # Store median in temporary file
        median_output = open("median_modular_2.data", "w")
        median_modular_vpn = statistics.median_low(self._totals_modular_vpn)
        print("4 {} {}".format(str(median_modular_vpn), str(self.rounded_div(median_modular_vpn, self._min_actions_vpn))), file=median_output)
        median_output.close()

        # Store action distribution in a temporary file
        action_distribution_cto = open("action_distribution_vpn.data", "w")

        reverse_mapping = [None] * len(test_mapping)
        for k, v in test_mapping.items():
            reverse_mapping[v] = k

        for i in range(0, len(reverse_mapping)):
            action_distribution_cto.write(reverse_mapping[i] + " ")
        action_distribution_cto.write("\n")

        for j in range(0, number_of_runs):
            for i in range(0, len(test_mapping)):
                action_distribution_cto.write(str(test_attempts[i][j] / test_totals[j]) + " ")
            action_distribution_cto.write("\n")

        action_distribution_cto.close()

    def test_0006_employee_scenario_omniscient(self) -> None:

        # short-circuiting the test
        if not enabled_tests["test_0006_employee_scenario_omniscient"]:
            return

        # Scenario prerequisite - the attacker successfully spearfished the employee and got a session onto their PC
        #                       - the session is manually generated because spearphishng is not implemented as is
        #                         requires user modelling
        #                       - routing issues (i.e. not being able to pass from perimeter router to infrastructure
        #                         router) are resolved by the session, which models reverse shell following the
        #                         spearphishing exploit
        s = self._env.add_session("attacker", ["attacker_node", "perimeter_router", "infrastructure_router", "emp_pc"])

        # Scenario unfolding:
        # - T1003 Credentials Dumping (extracting authorization of employee useful for the web server)
        # - T1018 Remote System Discovery (scanning of other infrastructure networks, identifying services)
        # -       Connection to the web server via rdp
        # - T1003 Credentials Dumping (extracting authorization of employee to the dc from the local system)
        # - T1018 Remote System Discovery (scanning of other infrastructure networks, identifying services, discovering
        #         path to SRV network segment)
        # -       Connection to the dc
        # - T1088 User privilege escalation to Administrator account
        # - T1097 Creating the golden ticket
        # -       Connection to the db server using the golden ticket
        # - T1005 Data from local system (access to private data using CTO's credentials

        # --------------------------------------------------------------------------------------------------------------
        # Credentials Dumping
        action = self._actions["rit:active_recon:information_discovery"]

        e = ExploitStore().get_exploit(service="powershell", category=ExploitCategory.AUTH_MANIPULATION)[0]
        action.set_exploit(e)

        self._attacker.execute_action("10.0.2.3", "powershell", action, session=s)

        self._env.run()

        message = self._attacker.get_last_response()

        self.assertEqual(message.status, Status(StatusOrigin.SERVICE, StatusValue.SUCCESS))

        emp_auth = None
        for a in message.content:
            if a.identity == "employee":
                emp_auth = a
                break

        self.assertIsNotNone(emp_auth, "Got authorization for an employee")

        # --------------------------------------------------------------------------------------------------------------
        # Remote System Discovery
        #
        # Get a list of hosts reachable from the employee's computer
        action = self._actions["rit:active_recon:host_discovery"]

        nets_to_scan = [IPNetwork("10.0.0.0/24"), IPNetwork("10.0.1.0/24"), IPNetwork("10.0.2.0/24")]

        live_machines = []
        for net in nets_to_scan:
            for host in net.iter_hosts():
                self._attacker.execute_action(str(host), "", action, session=s)
                result, _ = self._env.run()

                self.assertTrue(result, "Environment correctly processed the request")

                message = self._attacker.get_last_response()

                if message.status == Status(StatusOrigin.NODE, StatusValue.SUCCESS):
                    live_machines.append(message.src_ip)

        # Identify services on accessible machines
        action = self._actions["rit:active_recon:service_discovery"]
        mssql_ip = None
        websrv_ip = None

        for machine in live_machines:
            self._attacker.execute_action(str(machine), "", action, session=s)
            result, _ = self._env.run()

            self.assertTrue(result, "Environment correctly processed the request")

            message = self._attacker.get_last_response()

            if "mssql" in message.content:
                mssql_ip = machine

            elif "iis" in message.content:
                websrv_ip = machine

        self.assertIsNone(mssql_ip, "The DB server was not accessible")
        self.assertIsNotNone(websrv_ip, "The DB server was not accessible")

        # --------------------------------------------------------------------------------------------------------------
        # Connection to the web server using employee authentication
        action = self._actions["rit:ensure_access:command_and_control"]

        self._attacker.execute_action(str(websrv_ip), "rdp", action, authorization=emp_auth, session=s)

        result, _ = self._env.run()
        self.assertTrue(result, "Environment correctly processed the request")

        message = self._attacker.get_last_response()
        # New session established to the web server
        web_session = message.session

        # This is going to be more sensible, when there is a visibility separation in session elements (i.e., user
        # sees only the IP addresses along the session)
        self.assertEqual(web_session.endpoint.id, "web_srv", "Successfully connected to the web server")

        # --------------------------------------------------------------------------------------------------------------
        # Credentials Dumping
        action = self._actions["rit:active_recon:information_discovery"]

        e = ExploitStore().get_exploit(service="powershell", category=ExploitCategory.AUTH_MANIPULATION)[0]
        action.set_exploit(e)

        self._attacker.execute_action(str(websrv_ip), "powershell", action, session=web_session)

        self._env.run()

        message = self._attacker.get_last_response()

        self.assertEqual(message.status, Status(StatusOrigin.SERVICE, StatusValue.SUCCESS))

        dc_auth = None
        for a in message.content:
            if a.identity == "employee":
                dc_auth = a
                break

        self.assertIsNotNone(dc_auth, "Got DC authorization for an employee")

        # --------------------------------------------------------------------------------------------------------------
        # Remote System Discovery
        #
        # Get a list of hosts reachable from the web server (it should be different than from an employee computer)
        action = self._actions["rit:active_recon:host_discovery"]

        nets_to_scan = [IPNetwork("10.0.0.0/24"), IPNetwork("10.0.1.0/24"), IPNetwork("10.0.2.0/24")]

        live_machines = []
        for net in nets_to_scan:
            for host in net.iter_hosts():
                self._attacker.execute_action(str(host), "", action, session=web_session)
                result, _ = self._env.run()

                self.assertTrue(result, "Environment correctly processed the request")

                message = self._attacker.get_last_response()

                if message.status == Status(StatusOrigin.NODE, StatusValue.SUCCESS):
                    live_machines.append(message.src_ip)

        # Identify services on accessible machines
        action = self._actions["rit:active_recon:service_discovery"]
        mssql_ip = None
        dc_ip = None

        for machine in live_machines:
            self._attacker.execute_action(str(machine), "", action, session=web_session)
            result, _ = self._env.run()

            self.assertTrue(result, "Environment correctly processed the request")

            message = self._attacker.get_last_response()

            if "mssql" in message.content:
                mssql_ip = machine

            elif "windows_server_2019" in message.content:
                dc_ip = machine

        self.assertIsNotNone(mssql_ip, "The DB server is now accessible")
        self.assertIsNotNone(dc_ip, "The DC server is now accessible")

        # --------------------------------------------------------------------------------------------------------------
        # Connection to the web server using employee authentication
        action = self._actions["rit:ensure_access:command_and_control"]

        self._attacker.execute_action(str(dc_ip), "rdp", action, authorization=dc_auth, session=web_session)

        result, _ = self._env.run()
        self.assertTrue(result, "Environment correctly processed the request")

        message = self._attacker.get_last_response()
        # New session established to the DC server
        dc_session = message.session

        self.assertEqual(dc_session.endpoint.id, "dc_srv", "Successfully connected to the web server")

        # --------------------------------------------------------------------------------------------------------------
        # Root privilege escalation to get full access to the DC
        action = self._actions["rit:privilege_escalation:root_privilege_escalation"]

        exploits = ExploitStore().get_exploit(service="powershell", category=ExploitCategory.AUTH_MANIPULATION)
        exploit = None

        # Get the exploit which enables elevated access
        for e in exploits:
            for p in e.parameters.values():
                if p.exploit_type == ExploitParameterType.ENABLE_ELEVATED_ACCESS and p.value == "TRUE":
                    exploit = e
                    break

        self.assertIsNotNone(exploit, "Found an exploit which enables elevation to root privileges")

        action.set_exploit(exploit)
        self._attacker.execute_action(str(dc_ip), "powershell", action, authorization=dc_auth, session=dc_session)

        result, _ = self._env.run()
        self.assertTrue(result, "Environment correctly processed the request")

        message = self._attacker.get_last_response()

        dc_root_auth = message.authorization

        self.assertEqual(dc_root_auth.identity, "Administrator", "Got administrative access to the dc server")

        # --------------------------------------------------------------------------------------------------------------
        # Get root access to the entire infrastructure
        action = self._actions["rit:privilege_escalation:root_privilege_escalation"]

        e = ExploitStore().get_exploit(service="windows_server_2019", category=ExploitCategory.AUTH_MANIPULATION)[0]
        action.set_exploit(e)

        self._attacker.execute_action(str(dc_ip), "windows_server_2019", action, authorization=dc_root_auth, session=dc_session)

        result, _ = self._env.run()
        self.assertTrue(result, "Environment correctly processed the request")

        message = self._attacker.get_last_response()

        infrastructure_root_auth = message.authorization

        self.assertEqual(infrastructure_root_auth.nodes, ["*"], "Got administrative access to the infrastructure")

        # --------------------------------------------------------------------------------------------------------------
        # Data exfiltration
        action = self._actions["rit:disclosure:data_exfiltration"]

        self._attacker.execute_action(str(mssql_ip), "mssql", action, session=web_session, authorization=infrastructure_root_auth)

        result, _ = self._env.run()

        self.assertTrue(result, "Environment correctly processed the request")

        message = self._attacker.get_last_response()

        data_found = False
        for datum in message.content:
            if datum.description == "Secret data":
                data_found = True
            break

        self.assertTrue(data_found, "Successfully extracted data from the DB")

    def test_0007_employee_scenario_random(self) -> None:

        # Configuration for evaluation
        filename = "employee_scenario_random.data"

        # Short-circuit scenario
        if not enabled_tests["test_0007_employee_scenario_random"]:
            return

        output = open(filename, "w")

        s = self._env.add_session("attacker", ["attacker_node", "perimeter_router", "infrastructure_router", "emp_pc"])

        # --------------------------------------------------------------------------------------------------------------
        # Let random attacker attempt the complex scenario
        for i in range(0, number_of_runs):
            self._random_attacker.reset()
            # --------------------------------------------------------------------------------------------------------------
            # Let random attacker attempt the simple scenario
            self._random_attacker.set_action_limit(150000)
            self._random_attacker.set_action_namespace("rit")
            self._random_attacker.add_targets(IPNetwork("10.0.0.0/24"))
            self._random_attacker.add_targets(IPNetwork("10.0.1.0/24"))
            self._random_attacker.add_targets(IPNetwork("10.0.2.0/24"))
            self._random_attacker.add_sessions(s)
            self._random_attacker.set_services("postfix", "bash", "apache", "skysea_client_view",
                                               "windows_server_2019",
                                               "rdp", "mssql", "powershell")

            self._random_attacker.set_reduction_strategy(ReductionStrategy.LIVE_TARGETS_ONLY)
            self._random_attacker.set_goal("Secret data")

            self._random_attacker.run()
            self._env.run()

            total = self._random_attacker.attack_stats()[0].total
            print("5 {} {}".format(str(total), str(self.rounded_div(total, self._min_actions_employee))), file=output)
            print("Employee scenario [random attacker]: run no. {}/{}, total actions: {}".format(str(i+1), str(number_of_runs), str(total)))
            self._totals_random_employee.append(total)

        output.close()

        # Store median in temporary file
        median_output = open("median_random_3.data", "w")
        median_random_employee = statistics.median_low(self._totals_random_employee)
        print("5 {} {}".format(str(median_random_employee), str(self.rounded_div(median_random_employee, self._min_actions_employee))), file=median_output)
        median_output.close()

    def test_0008_employee_scenario_modular(self) -> None:

        # Configuration for evaluation
        filename = "employee_scenario_modular.data"

        # Short-circuit scenario
        if not enabled_tests["test_0008_employee_scenario_modular"]:
            return

        # give the attacker the session and IP of a target
        attacked_ips = ["10.0.2.3"]
        s = self._env.add_session("attacker", ["attacker_node", "perimeter_router", "infrastructure_router", "emp_pc"])

        output = open(filename, "w")

        test_mapping = None
        test_attempts = {}
        test_totals = []

        for i in range(0, number_of_runs):
            self._modular_attacker.reset()

            for ip in attacked_ips:
                self._modular_attacker._memory.new_host(IPAddress(ip))
                self._modular_attacker._memory.new_session(IPAddress(ip), s)

            # tell the attacker IPs of the routers - sadly currently unobtainable via simulator
            self._modular_attacker._submodules[0]._targeter.add_router_manually(IPNetwork("10.0.0.1/24"))
            self._modular_attacker._submodules[0]._targeter.add_router_manually(IPNetwork("10.0.1.1/24"))
            self._modular_attacker._submodules[0]._targeter.add_router_manually(IPNetwork("10.0.2.1/24"))

            # let the attacker do its thing until it finds the secret data
            actions = 0
            while (True):
                actions += 1
                self._modular_attacker.run()
                _, _ = self._env.run() if self._env._state == EnvironmentState.PAUSED else self._env.run()
                if len(self._modular_attacker._memory.all_data()) > 0:
                    break

            print("6 {} {}".format(str(actions), str(self.rounded_div(actions, self._min_actions_employee))), file=output)
            print("Employee scenario [modular attacker]: run no. {}/{}, total actions: {}".format(str(i+1), str(number_of_runs), str(actions)))
            self._totals_modular_employee.append(actions)

            # Get some statistics on action ratios
            mapping, attempts = self._modular_attacker.action_stats()

            if test_mapping is None:
                test_mapping = mapping

            if len(mapping) > len(test_mapping):
                raise Exception("Got longer mapping than before")

            for j in range(0, len(test_mapping)):
                if j not in test_attempts:
                    test_attempts[j] = []
                test_attempts[j].append(0)

            for item, index in mapping.items():
                test_attempts[test_mapping[item]][-1] = attempts[index]

            test_totals.append(actions)

        output.close()

        # Store median in temporary file
        median_output = open("median_modular_3.data", "w")
        median_modular_employee = statistics.median_low(self._totals_modular_employee)
        print("6 {} {}".format(str(median_modular_employee), str(self.rounded_div(median_modular_employee, self._min_actions_employee))), file=median_output)
        median_output.close()

        # Store action distribution in a temporary file
        action_distribution_cto = open("action_distribution_employee.data", "w")

        reverse_mapping = [None] * len(test_mapping)
        for k, v in test_mapping.items():
            reverse_mapping[v] = k

        for i in range(0, len(reverse_mapping)):
            action_distribution_cto.write(reverse_mapping[i] + " ")
        action_distribution_cto.write("\n")

        for j in range(0, number_of_runs):
            for i in range(0, len(test_mapping)):
                action_distribution_cto.write(str(test_attempts[i][j] / test_totals[j]) + " ")
            action_distribution_cto.write("\n")

        action_distribution_cto.close()

    def test_0009_simple_scenario_random_strategies(self) -> None:

        # Configuration for evaluation
        filename = "random_strategies.data"

        # Short-circuit scenario
        if not enabled_tests["test_0009_simple_scenario_random_strategies"]:
            return

        output = open(filename, "w")

        s = self._env.add_session("attacker", ["attacker_node", "perimeter_router", "infrastructure_router", "cto_pc"])

        # --------------------------------------------------------------------------------------------------------------
        # Let random attacker attempt the simple scenario
        strategies = [ReductionStrategy.NO_STRATEGY, ReductionStrategy.NO_DUPLICATE_ACTIONS,
                      ReductionStrategy.KNOWN_SERVICES_ONLY, ReductionStrategy.LIVE_TARGETS_ONLY,
                      ReductionStrategy.NO_DUPLICATE_ACTIONS | ReductionStrategy.KNOWN_SERVICES_ONLY,
                      ReductionStrategy.NO_DUPLICATE_ACTIONS | ReductionStrategy.LIVE_TARGETS_ONLY,
                      ReductionStrategy.KNOWN_SERVICES_ONLY | ReductionStrategy.LIVE_TARGETS_ONLY,
                      ReductionStrategy.KNOWN_SERVICES_ONLY | ReductionStrategy.LIVE_TARGETS_ONLY | ReductionStrategy.NO_DUPLICATE_ACTIONS]

        for i in range(0, number_of_runs):
            print("Run no. {}".format(str(i)))
            for strat in strategies:
                self._random_attacker.reset()
                # --------------------------------------------------------------------------------------------------------------
                # Let random attacker attempt the simple scenario
                self._random_attacker.set_action_limit(150000)
                self._random_attacker.set_action_namespace("rit")
                self._random_attacker.add_targets(IPNetwork("10.0.0.0/24"))
                self._random_attacker.add_targets(IPNetwork("10.0.1.0/24"))
                self._random_attacker.add_targets(IPNetwork("10.0.2.0/24"))
                self._random_attacker.add_sessions(s)
                self._random_attacker.set_services("postfix", "bash", "apache", "skysea_client_view", "windows_server_2019",
                                                   "rdp", "mssql", "powershell")

                self._random_attacker.set_reduction_strategy(strat)
                self._random_attacker.set_goal("Secret data")

                self._random_attacker.run()
                self._env.run()

                total = self._random_attacker.attack_stats()[0].total
                output.write("{} ".format(str(total)))
                print("-- strategy: {}, actions: {}".format(str(strat), str(total)))
            output.write("\n")

        output.close()