set style fill solid 0.5 border -1
set style boxplot outliers pointtype 7
set style data boxplot
set pointsize 0.5
set boxwidth 0.25
unset key
set border 2
set xrange[0:7]
set yrange[10:1200]
set ytics(10, 50, 100, 500, 1000, 5000, 10000, 50000, 100000, 150000)
set xtics("CTO\nLearning" 1, "CTO\nLearning\nNormalized" 2, "VPN\nLearning" 3, "VPN\nLearning\nNormalized" 4, "Employee\nLearning" 5, "Employee\nLearning\nNormalized" 6)
set bmargin 5
set logscale y
set ytics nomirror
set xtics nomirror
set grid ytics noxtics
plot "cto_scenario_modular.data" using ($1-1):2, "cto_scenario_modular.data" using 1:3, "vpn_scenario_modular.data" using ($1-1):2, "vpn_scenario_modular.data" using 1:3, "employee_scenario_modular.data" using ($1-1):2, "employee_scenario_modular.data" using 1:3, "median_modular.data" using ($1-1):2 with linespoints lw 3 lt rgb "black" dt 3 pt 1, "median_modular.data" using 1:3 with linespoints lw 3 lt rgb "black" dt 3 pt 1