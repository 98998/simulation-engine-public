set style fill solid 0.5 border -1
set style boxplot outliers pointtype 7
set style data boxplot
set pointsize 0.5
set boxwidth 0.25
unset key
set border 2
set xrange[0:5]
set yrange[1000:150000]
set ytics(1000, 5000, 10000, 50000, 100000, 150000)
set xtics("No reduction\nstrategy" 1, "Known services" 2, "Live machines" 3, "Live machines +\nKnown services" 4)
set bmargin 5
set logscale y 10
set ytics nomirror
set xtics nomirror
set grid ytics noxtics
plot "random_strategies.data" using (1):1, "" using (2):3, "" using (3):4, "" using (4):7